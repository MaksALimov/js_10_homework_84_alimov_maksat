const express = require('express');
const router = express.Router();
const Task = require('../models/Task');
const User = require('../models/User');
const auth = require("../middleware/auth");
const checkStatus = require('../middleware/checkStatus');

const findUserTasks = async token => {
    const user = await User.findOne({token});
    return Task.find({user: user._id});
};

router.get('/', auth, async (req, res) => {
    try {
        const userTasks = await findUserTasks(req.token);
        res.send(userTasks);
    } catch {
        res.sendStatus(500);
    }
});

router.post('/', [auth, checkStatus], async (req, res) => {
    if (!req.body.username || !req.body.title || !req.body.status) {
        return res.status(400).send({error: 'Data not valid'});
    }

    const tasksData = {
        user: req.body.username,
        title: req.body.title,
        description: req.body.description || null,
        status: req.body.status,
    };

    const task = new Task(tasksData);

    try {
        await task.save();
        res.send(task);
    } catch {
        res.sendStatus(500);
    }
});

router.put('/:id', [auth, checkStatus], async (req, res) => {
    if (!req.body.title || !req.body.status) {
        return res.status(400).send({error: 'Data not valid'});
    }

    try {
        const userTasks = await findUserTasks(req.token);

        if (userTasks.some(task => String(task._id) === req.params.id)) {
            Task.findByIdAndUpdate(
                req.params.id,
                {
                    title: req.body.title,
                    description: req.body.description,
                    status: req.body.status
                }, {new: true},
                (err, updatedTask) => err ? res.sendStatus(500) : res.send(updatedTask));

        } else {
            return res.status(403).send({error: 'You cannot edit other people\'s tasks'});
        }
    } catch {
        res.sendStatus(500);
    }
});

router.delete('/:id', auth, async (req, res) => {
    try {
        const userTasks = await findUserTasks(req.token);

        if (userTasks.some((task => String(task._id) === req.params.id))) {
            const task = await Task.findByIdAndDelete(req.params.id);

            if (task) {
                res.send(`Task ${task._id} removed`);
            }

        } else {
            res.status(404).send({error: 'Task not found'});
        }
    } catch {
        res.sendStatus(505);
    }
});

module.exports = router;