const express = require('express');
const router = express.Router();
const User = require('../models/User');

router.post('/', async (req, res) => {
    if (!req.body.password || !req.body.username) {
        return res.status(400).send({error: 'Data not valid'});
    }

    const usersData = {
        username: req.body.username,
        password: req.body.password,
    };

    const allUsers = await User.find();
    if (allUsers.some(user => user.username === usersData.username)) {
        return res.send({error: 'This username already exists'});
    }

    const user = new User(usersData);

    try {
        user.generateToken();
        await user.save();
        res.send(user);
    } catch {
        res.status(500);
    }
});

router.post('/sessions', async (req, res) => {
    const user = await User.findOne({username: req.body.username});

    if (!user) {
        return res.status(401).send({error: 'User not found'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(401).send({error: 'Password is incorrect'});
    }

    try {
        user.generateToken();
        await user.save();
        res.send({token: user.token});
    } catch {
        res.status(500);
    }
});

module.exports = router;