const checkStatus = (req, res, next) => {
    const taskStatus = ['new', 'in_progress', 'complete'];

    if (!taskStatus.some(status => status === req.body.status)) {
        return res.send({error: 'Status must be only on of: new || in_progress || complete'});
    }

    next();
};

module.exports = checkStatus;